# This hotspot is found in the trains operated by Dutch railway company NS
function connect {
  response=$(curl --write-out %{http_code} --silent --output /dev/null http://www.nstrein.ns.nl/?action=nstrein:main.internet)
  if [ "$response" == "200" ]; then
    echo "true"
  else
    echo "false"
  fi
}
