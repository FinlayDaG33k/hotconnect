# Hotconnect
Hotconnect is a little bash script that will attempt to log you in automatically into supported Wifi hotspots.  
It will try to "circumvent" captive portals by automating the requests that would normally send behind-the-scenes.  
After the captive portal is circumvented, Hotconnect will try to ping `1.1.1.1` and `stackoverflow.com` to check if a full connection has been established.  

Please do note that I (obviously) cannot add or verify all the hotspots all the time so feel free to open a PR to help me out!  
Just make sure you only add public hotspots that do not require credentials (or ask the user to enter them upon runtime).  
**never hardcode any credentials**

# Usage
Usage of Hotconnect is very simple.  
Just clone or download the repo, connect to the hotspot and run the `hotconnect.sh` from your terminal.  

# Adding support for a Hotspot
Adding support for a hotspot is done by creating a new file with the SSID of the hotspot inside the `hotspots` directory.

> myssid.sh

In this file, you create a new function called `connect`, in which you add the required code to connect to the hotspot:
```sh
function connect {
  response=$(curl --write-out %{http_code} --silent --output /dev/null http://my.portal.local/login)
  if [ "$response" == "200" ]; then
    echo "true"
  else
    echo "false"
  fi
}
```

Just make sure to keep note of the following things:
- Keep dependencies to a bare minimum (preferably curl only). This script is intended to run on all linux distros running bash.
- A `connect` function should always echo `true` or `false` (as seen in the example) based on whether the connection was successful or not

