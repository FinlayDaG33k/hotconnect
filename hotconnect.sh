#!/bin/bash

clear
echo -e "+--------------------------------------------------------------+"
echo -e "|                                                              |"
echo -e "|   \e[92m|   | +---+ --+-- +--- +---+ +---+ +---+ +--- +--- --+--   \e[97m|"
echo -e "|   \e[92m+---+ |   |   |   |    |   | |   | |   | +--- |      |     \e[97m|"
echo -e "|   \e[92m|   | +___+   |   +___ +___+ |   | |   | +___ +___   |     \e[97m|"
echo -e "|                                                              |"
echo -e "|      \e[31mNobody has time for portals holding you captive!        \e[97m|"
echo -e "|                                                              |"
echo -e "|      Made by Aroop \"FinlayDaG33k\" Roelofs                    |"
echo -e "|      Scripts contributed by community                        |"
echo -e "|      Licensed under the \e[1mGNU GPLv2\e[0m                            |"
echo -e "|      Visit https://www.finlaydag33k.nl/ for more stuff!      |"
echo -e "|                                                              |"
echo -e "+--------------------------------------------------------------+"

# Make sure curl is installed
curl=$(command -v curl)
if [ ! -x "${curl}" ]; then
  echo "[ERROR] Docker not found, please install it!"
  exit 1
fi

# Check the SSID we're connected to
ssid=$(iwgetid -r)
echo "[INFO] Found connection to \"${ssid}\""

# Make sure we support the SSID
if [ ! -f "./hotspots/${ssid}.sh" ]; then
  echo "[ERR] Connection file for \"${ssid}\" could not be found (it may not be supported yet)"
  exit 1
fi

# Load the connection file
. "hotspots/${ssid}.sh"

# Run the connection function
status=$(connect)
if [ "$(connect)" != "true" ]; then
  echo "[ERR] Connection failed!"
  exit 1
fi

# Check if the connection was a success
echo -n "[INFO] Checking internet status... "
if ping -c 1 1.1.1.1 &> /dev/null
then
  echo "Success!"
else
  echo "Failure!"
  exit 1
fi

echo -n "[INFO] Checking DNS status... "
if ping -c 1 stackoverflow.com &> /dev/null
then
  echo "Success!"
else
  echo "Failure!"
  exit 1
fi

echo "[INFO] Your internet should be all good to go!"